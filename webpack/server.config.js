import path from 'path';
import nodeExternals from 'webpack-node-externals';

export default (env, argv) => {
    const SERVER_PATH = `./src/server/server-${ argv.mode === 'production' ? 'prod' : 'dev' }.js`;

    return ({
        entry: {
            server: SERVER_PATH,
        },
        output: {
            path: path.resolve(__dirname, '../dist'),
            publicPath: '/',
            filename: '[name].js'
        },
        mode: argv.mode,
        target: 'node',
        devtool: '#source-map',
        node: {
            // Need this when working with express, otherwise the build fails
            __dirname: false,   // if you don't put this is, __dirname
            __filename: false,  // and __filename return blank or /
        },
        externals: [nodeExternals()], // Need this to avoid error when working with Express
        module: {
            rules: [
                {
                    // Transpiles ES6-8 into ES5
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                          presets: ['@babel/preset-env'],
                          plugins: [
                            ["@babel/transform-runtime", {
                              "regenerator": true,
                            }],
                          ],
                        }
                    }
                }
            ]
        }
    })
}