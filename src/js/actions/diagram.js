import {
    nodeDataAction, getNodeDataState,
    linkDataAction, getLinkDataState
} from '../stores/diagramModel';
import { getPaletteNodeTplState } from '../stores/paletteModel';
import { addDataReducer, removeDataReducer } from '../reducers';
import { getRandomColor, paletteNodeColor, buildGraphLinksModel } from '../shared';
import fetch from 'cross-fetch';

const addNodeData = nodeDataAction('addNodeData', addDataReducer);
const addLinkData = linkDataAction('addLinkData', addDataReducer);

export const addNode = targetNode => dispatch => {
    if (targetNode) {
        return dispatch(addNodeData(targetNode));
    }
    const nodeToAdd = ({ key: targetNode, label: targetNode, color: getRandomColor() });

    return dispatch(addNodeData(nodeToAdd));
};

export const addLink = targetLink => (dispatch, getState) => {
    const linkDataState = getLinkDataState(getState());
    const linkToAddIndex = linkDataState.findIndex(
        link => link.from === targetLink.from && link.to === targetLink.to
    );
    if (linkToAddIndex > -1) {
        return;
    }
    return dispatch(addLinkData({ from: targetLink.from, to: targetLink.to }));
};

const removeNodeData = nodeDataAction('removeNodeData', removeDataReducer);
export const removeNode = targetNode => (dispatch, getState) => {
    const nodeDataState = getNodeDataState(getState());
    const nodeToRemoveIndex = nodeDataState.findIndex(node => node.key === targetNode);
    if (nodeToRemoveIndex === -1) {
        return;
    }

    return dispatch(removeNodeData(nodeToRemoveIndex));
};

const removeLinkData = linkDataAction('removeLinkData', removeDataReducer);
export const removeLink = targetLink => (dispatch, getState) => {
    const linkDataState = getLinkDataState(getState());
    const linkToRemoveIndex = linkDataState.findIndex(
        link => link.from === targetLink.from && link.to === targetLink.to
    );
    if (linkToRemoveIndex === -1) {
        return;
    }

    return dispatch(removeLinkData(linkToRemoveIndex));
};

const addColorForNode = nodeDataArray =>
    nodeDataArray.map(node => Object.assign(node, { color: paletteNodeColor[node.category] }));
const updateNodeData = nodeDataAction('updateNodeData', (_state, nodeDataArray) => [...nodeDataArray]);
const updateLinkData = linkDataAction('updateLinkData', (_state, linkDataArray) => [...linkDataArray]);
export const loadDemoData = demoData => async (dispatch, getState) => {
    const paletteNodeTplState = getPaletteNodeTplState(getState());
    let { nodeDataArray = [], linkDataArray = [] } = {};
    if(!demoData) {
        ({ nodeDataArray, linkDataArray } = await fetch("/api/waterfall").then(res => res.json()));
    } else {
        ({ nodeDataArray, linkDataArray } = buildGraphLinksModel(demoData));
    }
    nodeDataArray.forEach(node => {
        const target = paletteNodeTplState.find(e => e.label === node.label);
        if (target) {
            node.id = target.id;
        }
    });
    dispatch(updateNodeData(addColorForNode(nodeDataArray)));
    dispatch(updateLinkData(linkDataArray));
};