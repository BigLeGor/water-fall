import { getSelectedNodeKeys, bindSelectNodeReducer } from '../stores/selectedNodeKeys';
import { addDataReducer, removeDataReducer } from '../reducers';

const selectNode = bindSelectNodeReducer((state, target) => ({ ...state, ...target }));
export const nodeSelected = targetNode => (dispatch, getState) => {
    const selectedNodeKeysState = getSelectedNodeKeys(getState());
    if (!Object.keys(selectedNodeKeysState).find(key => key === targetNode)) {
        dispatch(selectNode(targetNode));
    }
};

const deselectNode = bindSelectNodeReducer(() => ({}));
export const nodeDeselected = (nodeKey = "") => (dispatch, getState) => {
    const selectedNodeKeysState = getSelectedNodeKeys(getState());
    const targetNode = selectedNodeKeysState[nodeKey];
    if (targetNode) {
        dispatch(deselectNode());
    }
};