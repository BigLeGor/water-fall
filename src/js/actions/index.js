import { getNodeDataState, getLinkDataState } from '../stores/diagramModel';
import { getPaletteBlockPropsState } from '../stores/paletteModel';
import {
    addNode, removeNode,
    addLink, removeLink,
    loadDemoData
} from './diagram';
import {
    getNodePropsState,
    updateTargetNodeProps
} from './nodeProps';

export { nodeSelected, nodeDeselected } from './selectedNodes';

export {
    getPaletteModel,
    loadNodeTemplateMap
} from './palette';

export const getFullDiagramNodes = state => {
    const linkDataState = getLinkDataState(state);
    const diagramNodes = getNodeDataState(state);
    const paletteNodeBlockProps = getPaletteBlockPropsState(state);
    const getUpdatedNodeBlockProps = nodeKey => getNodePropsState(state, nodeKey);
    

    return diagramNodes.map(({ key: nodeKey, id: propId, label: Label, blockType: BlockType, category: Category, ...dNode }) => {
        const updatedBlockProps = getUpdatedNodeBlockProps(nodeKey) || {};
        const defaultBlockProps = paletteNodeBlockProps[propId] || {};

        let diagramNode = {
            ID: nodeKey,
            Label,
            BlockType,
            Category,
            ...dNode,
            ...defaultBlockProps,
            ...updatedBlockProps
        };

        const {
            FromEntities: originFromEntities,
            ToEntities: originToEntities
        } = defaultBlockProps
        const {
            FromEntities: updatedFromEntities,
            ToEntities: updatedToEntities
        } = updatedBlockProps;

        if (updatedFromEntities) {
            diagramNode = {
                ...diagramNode,
                FromEntities: [
                    updatedFromEntities
                ]
            };
        } else if (originFromEntities) {
            const [defaultFromEntities] = originFromEntities;
            diagramNode = {
                ...diagramNode,
                FromEntities: [
                    defaultFromEntities
                ]
            };
        }

        if (updatedToEntities) {
            diagramNode = {
                ...diagramNode,
                ToEntities: [
                    updatedToEntities
                ]
            };
        } else if (originToEntities) {
            const [defaultToEntities] = originToEntities;
            diagramNode = {
                ...diagramNode,
                ToEntities: [
                    defaultToEntities
                ]
            };
        }

        const childSteps = linkDataState.filter(e => e.from === nodeKey).map(e => e.to);
        diagramNode = {
            ...diagramNode,
            ChildSteps: childSteps
        };

        return diagramNode;
    });
};

export {
    addNode, removeNode,
    addLink, removeLink,
    loadDemoData,
    updateTargetNodeProps
}