import { nodePropsAction, getNodePropsState } from '../stores/updatedNodeProps';

const updateNodeProps = nodePropsAction('updateNodeProps',
    (state, nodeProps) => ({
        ...state,
        ...nodeProps
    })
);

const updateTargetNodeProps = (nodeKey, nodeProps) =>
    (dispatch) => {
        dispatch(updateNodeProps(nodeKey, {
            ...nodeProps
        }));
    };

export {
    updateTargetNodeProps,
    getNodePropsState
};