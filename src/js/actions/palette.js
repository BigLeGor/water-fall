import fetch from 'cross-fetch';
import {
    paletteModelAction,
    paletteNodeTplAction,
    paletteBlockPropsAction
} from '../stores/paletteModel';
import { paletteNodeColor } from '../shared';

export {
    getNodeDataState as getPaletteModel
} from '../stores/diagramModel';

const requestDealData = paletteModelAction('requestDealData', (state, isFetching) => ({ ...state, isFetching }));

const buildNodeTemplateMap = ({ category, label, id, blockType }) => ({ category, label, id, color: paletteNodeColor[category], blockType });
const buildBlockProp = ({ id, props }) => ({ [id]: { ...props } });

const handleDealData = dealData => {
    let blockProps = {};
    let nodeTpls = [];
    dealData.forEach(item => {
        blockProps = {
            ...blockProps,
            ...buildBlockProp(item)
        };

        nodeTpls = [
            ...nodeTpls,
            buildNodeTemplateMap(item)
        ];
    });

    return {
        blockProps,
        nodeTpls
    };
};

const initNodeTemplateMap = paletteNodeTplAction('initNodeTemplateMap',
    (state, nodeTpls) => ([
        ...state,
        ...nodeTpls
    ])
);

const initBlockProps = paletteBlockPropsAction('initBlockProps',
    (state, blockProp) => ({
        ...state,
        ...blockProp
    })
);

export const loadNodeTemplateMap = () => async dispatch => {
    dispatch(requestDealData(true));
    const dealData = await fetch("/api/deal-data").then(res => res.json());
    dispatch(requestDealData(false));
    const {
        blockProps,
        nodeTpls
    } = handleDealData(dealData);
    dispatch(initNodeTemplateMap(nodeTpls));
    dispatch(initBlockProps(blockProps));
};