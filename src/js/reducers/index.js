export const addDataReducer = (state, ...targets) => [...state, ...targets];

export const removeDataReducer = (state, targetIndex) => [
    ...state.slice(0, targetIndex),
    ...state.slice(targetIndex + 1)
];