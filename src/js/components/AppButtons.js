import React from 'react';
import { connect } from 'react-redux';
import { loadDemoData, addNode, getFullDiagramNodes } from '../actions';
import { saveFile, loadFile } from '../shared';

const buildWaterfall = ([, ...waterfallState]) => {
    const targetJsonArray = waterfallState.map(e => {
        const json = {};
        const verifiedKeys = Object.keys(e).filter(k => !k.startsWith('__'));
        verifiedKeys.forEach(k => json[k] = e[k]);
        return json;
    });
    const waterfallJsonStr = JSON.stringify(targetJsonArray);
    const bytes = new TextEncoder().encode(waterfallJsonStr);
    return new Blob([bytes], {
        type: "application/json;charset=utf-8"
    });
};

const mapStateToProps = state => ({
    saveToFile: () => {
        const nodeDataState = getFullDiagramNodes(state);
        saveFile(buildWaterfall(nodeDataState));
    }
});

const mapDispatchToProps = dispatch => {
    let nodeId = 0;
    return {
        initHandler: () => dispatch(loadDemoData()),
        addNodeHandler: () => {
            dispatch(addNode('node' + nodeId));
            nodeId += 1;
        },
        loadFromFile: async (e) => {
            const jsonObj = await loadFile(e);
            dispatch(loadDemoData(jsonObj));
        }
    };
};

let loadFileElem = React.createRef();

const AppButtons = ({ initHandler, addNodeHandler, saveToFile, loadFromFile }) => ((
    <div className="centered-container">
        <div className="inline-element">
            <button type="button" onClick={() => initHandler()}>
                Load demo diagram
            </button>
        </div>
        <div className="inline-element">
            <button type="button" onClick={() => saveToFile()}>
                Save to file
            </button>
        </div>
        <div className="inline-element">
            <input ref={loadFileElem}
                type="file" multiple="" style={{ display: "none" }}
                onChange={loadFromFile}
            />
            <button type="button" onClick={() => { loadFileElem.current.click() }}>
                Load from file
            </button>
        </div>
        <div className="inline-element">
            <button type="button" onClick={() => addNodeHandler()}>
                Add node with selected node(s) as parent(s)
            </button>
        </div>
    </div>
));

export default connect(mapStateToProps, mapDispatchToProps)(AppButtons);