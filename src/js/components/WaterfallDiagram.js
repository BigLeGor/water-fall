import React from 'react';
import PropTypes from 'prop-types';
import { GojsDiagram } from 'react-gojs';

import { compose } from '../shared';
import {
    addLinkTemplate,
    addNodeTemplate,
    addToolManager,
    createDiagram
} from './DiagramFns';
import '../../css/WaterfallDiagram.css';

const diagramCreators = (onNodeClick, onNodeSelection) => [
    addLinkTemplate,
    addNodeTemplate(onNodeClick, onNodeSelection),
    addToolManager,
    createDiagram
];

const WaterfallDiagram = ({ model, onModelChange, onNodeClick, onNodeSelection, createPalette }) => (
    <GojsDiagram
        diagramId="diagram-waterfall"
        model={model}
        createDiagram={
            compose(
                createPalette,
                ...diagramCreators(onNodeClick, onNodeSelection)
            )
        }
        className="diagram-waterfall"
        onModelChange={onModelChange}
    />
);

WaterfallDiagram.propTypes = {
    model: PropTypes.object,
    onModelChange: PropTypes.func,
    onNodeSelection: PropTypes.func,
    setPaletteNodeTemplate: PropTypes.func
};

export default WaterfallDiagram;