import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
    menu: {
        width: 200,
    },
    button: {
        margin: theme.spacing(1),
    },
}));

const buildTitle = title => (
    <List>
        <ListItem button key={title}>
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary={title} />
        </ListItem>
    </List>
);

const buildArrayTextField = setUpdatedProp => (title, sourceProp = [], nodeProp = {}) => {
    const classes = useStyles();
    return sourceProp.reduce((acc, curr, currIdx) => {
        const targetProp = nodeProp[currIdx] || {};
        const entities = Object.entries(curr).map(([key, entity]) => {
            return Array.isArray(entity) ?
                (
                    <TextField
                        id={`standard-select-${key}`}
                        select
                        label={key}
                        className={classes.textField}
                        value={entity.find(e => e === targetProp[key]) || entity[0]}
                        onChange={e => setUpdatedProp({ [currIdx]: { ...targetProp, [key]: e.target.value } })}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        helperText={`Please select your ${key}`}
                        margin="normal"
                    >
                        {entity.map((item, idx) => (
                            <MenuItem key={idx} value={item}>
                                {item}
                            </MenuItem>
                        ))}
                    </TextField>
                ) : (
                    <TextField
                        id={`standard-${key}`}
                        label={key}
                        className={classes.textField}
                        value={targetProp[key] || entity}
                        onChange={e => setUpdatedProp({ [currIdx]: { ...targetProp, [key]: e.target.value } })}
                        margin="normal"
                    />
                );
        });
        return [
            ...acc,
            ...entities
        ];
    }, [buildTitle(title)]);
};

const buildFromEntitiesEditor = (setNodeProps, nodeProps) => (FromEntities) => {
    const targetProp = nodeProps.FromEntities || {};
    const setUpdatedProp = updatedProp => setNodeProps({
        ...nodeProps,
        FromEntities: {
            ...targetProp,
            ...updatedProp
        }
    });
    return buildArrayTextField(setUpdatedProp)("From Entity", FromEntities, targetProp);
};

const buildToEntitiesEditor = (setNodeProps, nodeProps) => (ToEntities) => {
    const targetProp = nodeProps.ToEntities || {};
    const setUpdatedProp = updatedProp => setNodeProps({
        ...nodeProps,
        ToEntities: {
            ...targetProp,
            ...updatedProp
        }
    });
    return buildArrayTextField(setUpdatedProp)("To Entity", ToEntities, targetProp);
};

const buildNormalTextField = (setNodeProps, nodeProps) => (title, sourceProp = {}) => {
    const classes = useStyles();
    const [[key, defaultVal] = []] = Object.entries(sourceProp);
    const value = nodeProps[key] || defaultVal;
    const onChange = e => setNodeProps({ ...nodeProps, [key]: e.target.value });
    return (
        <TextField
            id={`standard-${key}`}
            label={title}
            className={classes.textField}
            value={value}
            onChange={onChange}
            margin="normal"
        />
    );
};

const buildObjectTextField = (setNodeProps, nodeProps) => (sourceProp = {}) => {
    const classes = useStyles();
    const [[key, entity] = []] = Object.entries(sourceProp);
    if (!entity) {
        return null;
    }

    const targetProp = nodeProps[key] || {};
    const title = buildTitle(key);

    const textFiled = Object.entries(entity).map(([entityKey, defaultVal] = []) => (
        <TextField
            id={`standard-${entityKey}`}
            label={entityKey}
            className={classes.textField}
            value={targetProp[entityKey] || defaultVal}
            onChange={e => setNodeProps({
                ...nodeProps,
                [key]: {
                    ...targetProp,
                    [entityKey]: e.target.value
                }
            })}
            margin="normal"
        />)
    );
    return [
        title,
        ...textFiled
    ];
};

const NodePropsEditor = React.memo(({
    selectedNodeKey,
    selectedNodeProps: {
        blockProps = {},
        updatedNodeProps = {}
    },
    onNodePropUpdate,
    onClose
}) => {
    const classes = useStyles();
    const open = Object.keys(blockProps).length > 0;
    const [nodeProps, setNodeProps] = React.useState({});
    const {
        FromEntities,
        ToEntities,
        Priority,
        HasCondition,
        AmoutFormula,
        ConditionalFormula,
        MaxAmount
    } = blockProps;

    const sideList = (
        <div
            className={classes.list}
            role="presentation"
        >
            {buildFromEntitiesEditor(setNodeProps, { ...updatedNodeProps, ...nodeProps })(FromEntities)}
            <Divider />
            {buildToEntitiesEditor(setNodeProps, { ...updatedNodeProps, ...nodeProps })(ToEntities)}
            <Divider />
            {buildNormalTextField(setNodeProps, { ...updatedNodeProps, ...nodeProps })("Priority", { Priority })}
            <Divider />
            {buildNormalTextField(setNodeProps, { ...updatedNodeProps, ...nodeProps })("HasCondition", { HasCondition })}
            <Divider />
            {buildObjectTextField(AmoutFormula)}
            <Divider />
            {buildObjectTextField(ConditionalFormula)}
            <Divider />
            {buildObjectTextField(MaxAmount)}
        </div>
    );

    const buttonBar = (
        <div>
            <Button variant="contained" className={classes.button} onClick={() => {
                setNodeProps({});
                onClose();
            }}>
                Cancel
            </Button>
            <Button variant="contained" color="primary" className={classes.button} onClick={() => {
                onNodePropUpdate(selectedNodeKey, nodeProps);
                setNodeProps({});
                onClose();
            }}>
                Save
            </Button>
        </div>
    );

    return (
        <Drawer anchor="right" open={open} onClose={() => {
            setNodeProps({});
            onClose();
        }}>
            {sideList}
            {buttonBar}
        </Drawer>
    );
});

export default NodePropsEditor;