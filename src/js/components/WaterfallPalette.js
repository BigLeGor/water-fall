import React, { Component } from 'react';
import go from 'gojs';

const paletteCreator = target => {
    target.createPalette = model => (...palettes) => diagramObj => {
        const { length } = model;
        if (length && length > 0) {
            palettes.forEach(({ paletteId, targetBlockType }) => {
                const myPalette = new go.Palette(paletteId);  // must name or refer to the DIV HTML element
                myPalette.model = new go.GraphLinksModel(
                    model.filter(m => m.blockType.toUpperCase() === targetBlockType.toUpperCase())
                );
                //myPalette.nodeTemplate = diagramObj.nodeTemplate;
                myPalette.nodeTemplateMap = diagramObj.nodeTemplateMap;
            });
        }
        return diagramObj;
    };
};

@paletteCreator
class WaterfallPalette extends Component {
    render() {
        return (
            <div id={this.props.paletteId} className="palette-item" />
        );
    }
}

export {
    WaterfallPalette as default
};