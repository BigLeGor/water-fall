import go, { Diagram, ToolManager } from 'gojs';

const withDiagramCheck = node => nodeFn => (...args) => {
    const { diagram } = node.findMainElement();
    if (!diagram) {
        return;
    }
    const { id: diagramId } = diagram.Ia;
    if (diagramId !== "diagram-waterfall") {
        return;
    }
    nodeFn(...args);
};

const addLinkTemplate = myDiagram => {
    const $ = go.GraphObject.make;
    myDiagram.linkTemplate = $(
        go.Link,  // the whole link panel
        {
            routing: go.Link.AvoidsNodes,
            curve: go.Link.JumpOver,
            corner: 5, toShortLength: 4,
            relinkableFrom: true,
            relinkableTo: true,
            reshapable: true,
            resegmentable: true,
            // mouse-overs subtly highlight links:
            mouseEnter: function (_e, link) { link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)"; },
            mouseLeave: function (_e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; },
            selectionAdorned: false
        },
        new go.Binding("points").makeTwoWay(),
        $(go.Shape,  // the highlight shape, normally transparent
            { isPanelMain: true, strokeWidth: 8, stroke: "transparent", name: "HIGHLIGHT" }),
        $(go.Shape,  // the link path shape
            { isPanelMain: true, stroke: "gray", strokeWidth: 2 },
            new go.Binding("stroke", "isSelected", function (sel) { return sel ? "dodgerblue" : "gray"; }).ofObject()),
        $(go.Shape,  // the arrowhead
            { toArrow: "standard", strokeWidth: 0, fill: "gray" }),
        $(go.Panel, "Auto",  // the link label, normally not visible
            { visible: false, name: "LABEL", segmentIndex: 2, segmentFraction: 0.5 },
            new go.Binding("visible", "visible").makeTwoWay(),
            $(go.Shape, "RoundedRectangle",  // the label shape
                { fill: "#F8F8F8", strokeWidth: 0 }),
            $(go.TextBlock, "Yes",  // the label
                {
                    textAlign: "center",
                    font: "10pt helvetica, arial, sans-serif",
                    stroke: "#333333",
                    editable: true
                },
                new go.Binding("text").makeTwoWay())
        )
    );
    return myDiagram;
};

const addNodeTemplate = (onNodeClick, onNodeSelection) => myDiagram => {
    const $ = go.GraphObject.make;
    myDiagram.nodeTemplateMap.add(
        "Start",
        $(
            go.Node,
            'Auto',
            $(go.Shape,
                'Circle',
                { strokeWidth: 0 },
                new go.Binding('fill', 'color'),
                { portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer" }
            ),
            $(go.TextBlock, { margin: 8, editable: false }, new go.Binding('text', 'label'))
        ));
    myDiagram.nodeTemplateMap.add(
        "Conditions",
        $(
            go.Node,
            'Auto',
            {
                doubleClick: (_e, node) => withDiagramCheck(node)(onNodeClick)(node.key, node.data.id, node.isSelected),
                selectionChanged: node => withDiagramCheck(node)(onNodeSelection)(node.key, node.isSelected),
                // the Node.location is at the center of each node
                locationSpot: go.Spot.Center
            },
            $(go.Shape,
                'RoundedRectangle',
                { minSize: new go.Size(120, 50), strokeWidth: 0 },
                new go.Binding('fill', 'color'),
                { portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer" }
            ),
            $(go.TextBlock, { margin: 8, editable: false }, new go.Binding('text', 'label'))
        ));
    myDiagram.nodeTemplateMap.add(
        "Executions",
        $(
            go.Node,
            'Auto',
            {
                doubleClick: (_e, node) => withDiagramCheck(node)(onNodeClick)(node.key, node.data.id, node.isSelected),
                selectionChanged: node => withDiagramCheck(node)(onNodeSelection)(node.key, node.isSelected),
                // the Node.location is at the center of each node
                locationSpot: go.Spot.Center
            },
            $(go.Shape,
                'RoundedRectangle',
                { minSize: new go.Size(120, 50), strokeWidth: 0 },
                new go.Binding('fill', 'color'),
                { portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer" }
            ),
            $(go.TextBlock, { margin: 8, editable: false }, new go.Binding('text', 'label'))
        ));
    myDiagram.nodeTemplateMap.add(
        "Assigns",
        $(
            go.Node,
            'Auto',
            {
                doubleClick: (_e, node) => withDiagramCheck(node)(onNodeClick)(node.key, node.data.id, node.isSelected),
                selectionChanged: node => withDiagramCheck(node)(onNodeSelection)(node.key, node.isSelected),
                // the Node.location is at the center of each node
                locationSpot: go.Spot.Center
            },
            $(go.Shape,
                'RoundedRectangle',
                { minSize: new go.Size(120, 50), strokeWidth: 0 },
                new go.Binding('fill', 'color'),
                { portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer" }
            ),
            $(go.TextBlock, { margin: 8, editable: false }, new go.Binding('text', 'label'))
        ));
    return myDiagram;
};

const addToolManager = myDiagram => {
    myDiagram.toolManager.panningTool.isEnabled = false;
    myDiagram.toolManager.mouseWheelBehavior = ToolManager.WheelScroll;
    //myDiagram.mouseDrop = (e, node) => console.log(node);
    return myDiagram;
};

const createDiagram = diagramId => {
    const $ = go.GraphObject.make;

    const myDiagram = $(go.Diagram, diagramId, {
        initialContentAlignment: go.Spot.LeftCenter,
        layout: $(go.TreeLayout, {
            angle: 90,
            arrangement: go.TreeLayout.ArrangementVertical,
            treeStyle: go.TreeLayout.StyleLayered
        }),
        isReadOnly: false,
        allowHorizontalScroll: true,
        allowVerticalScroll: true,
        allowZoom: false,
        allowSelect: true,
        autoScale: Diagram.Uniform,
        contentAlignment: go.Spot.TopCenter,
        "undoManager.isEnabled": true  // enable undo & redo
    });

    go.Shape.defineFigureGenerator("File", (_shape, w, h) => {
        const geo = new go.Geometry();
        const fig = new go.PathFigure(0, 0, true); // starting point
        geo.add(fig);
        fig.add(new go.PathSegment(go.PathSegment.Line, .75 * w, 0));
        fig.add(new go.PathSegment(go.PathSegment.Line, w, .25 * h));
        fig.add(new go.PathSegment(go.PathSegment.Line, w, h));
        fig.add(new go.PathSegment(go.PathSegment.Line, 0, h).close());
        const fig2 = new go.PathFigure(.75 * w, 0, false);
        geo.add(fig2);
        // The Fold
        fig2.add(new go.PathSegment(go.PathSegment.Line, .75 * w, .25 * h));
        fig2.add(new go.PathSegment(go.PathSegment.Line, w, .25 * h));
        geo.spot1 = new go.Spot(0, .25);
        geo.spot2 = go.Spot.BottomRight;
        return geo;
    });

    return myDiagram;
};

export {
    addLinkTemplate,
    addNodeTemplate,
    addToolManager,
    createDiagram
};