import React, { Component } from 'react';
import _ from 'lodash';
import {
  FormControl, FormControlLabel,
  Radio, RadioGroup, TextField,
  Divider, MenuItem, Checkbox, Switch
} from '@material-ui/core';

class ToEntity extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { title, selectedNodeProps, nodeProps, setNodeProps } = this.props;
    const { blockProps = {}, updatedNodeProps = {} } = selectedNodeProps;
    const { ToEntities, FromEntities } = blockProps;
    let CurrentOperator = _.isUndefined(FromEntities) ? 'Interest' : FromEntities[0].CurrentOperator
    if(nodeProps.FromEntities && nodeProps.FromEntities[0] && !_.isUndefined(nodeProps.FromEntities[0].CurrentOperator)){
      CurrentOperator = nodeProps.FromEntities[0].CurrentOperator
    }
    const showBond = CurrentOperator !== 'Interest'
    if(!ToEntities || !ToEntities.length) {
      return null;
    }
    return (
      <React.Fragment>
        { title }
        { this.buildToEntitiesEditor(setNodeProps, { ...updatedNodeProps, ...nodeProps })(ToEntities, showBond) }
      </React.Fragment>
    );
  }

  buildToEntitiesEditor = (setNodeProps, nodeProps) => (ToEntities, showBond) => {
    const targetProp = nodeProps.ToEntities || {};
    const setUpdatedProp = updatedProp => setNodeProps({
        ...nodeProps,
        ToEntities: {
            ...targetProp,
            ...updatedProp
        }
    });
    return this.buildSingleToEntity(setUpdatedProp)(ToEntities, targetProp, showBond);
  };

  buildSingleToEntity = setUpdatedProp => (sourceProp, nodeProp, showBond) => {
    const endTail = sourceProp.length;
    return sourceProp.map((item, index) => {
      const target = nodeProp[index] || {}
      return this.buildRadioField(setUpdatedProp, index, endTail)(item, target, showBond)
    })
  }

  buildRadioField = (setUpdatedProp, index, endTail) => (sourceProp, nodeProp, showBond) => {
    const { classes } = this.props;
    const radioDefaultValue = _.isUndefined(nodeProp.CurrentOperator) ? sourceProp.CurrentOperator : nodeProp.CurrentOperator
    const handleRadioChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "CurrentOperator": e.target.value }})
    return (
      <React.Fragment key={index}>
        <FormControl className={classes.verticalInit} >
          <RadioGroup value={radioDefaultValue} onChange = {handleRadioChange}>
            { this.renderBondInterest(setUpdatedProp, index)(sourceProp, nodeProp, radioDefaultValue) }
            { 
              showBond ? 
              this.renderBondPricipal(setUpdatedProp, index)(sourceProp, nodeProp, radioDefaultValue) : 
              this.renderColPrincipal(setUpdatedProp, index)(sourceProp, nodeProp, radioDefaultValue) 
            }
            { this.renderFee(setUpdatedProp, index)(sourceProp, nodeProp, radioDefaultValue) }
            { this.renderAccount(setUpdatedProp, index)(sourceProp, nodeProp, radioDefaultValue) }
          </RadioGroup>
        </FormControl>
        { endTail === index + 1 ? null : <Divider />}
      </React.Fragment>
    )
  };

  renderBondInterest = (setUpdatedProp, index) => (sourceProp, nodeProp, radioDefaultValue) => {
    const { classes } = this.props;
    const selectDefaultValue = _.isUndefined(nodeProp.BondID) ? sourceProp.BondID : nodeProp.BondID
    let inputDefaultValue = _.isUndefined(nodeProp.CouponCap) ? sourceProp.CouponCap : nodeProp.CouponCap
    const handleSelectChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "BondID": e.target.value }})
    const handleInputChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "CouponCap": e.target.value }})
    const entity = ['A', 'B', 'SUB']
    inputDefaultValue = inputDefaultValue || ''
    const selectControl = (
      <TextField
          select
          className={`${classes.verticalInit} ${classes.select}`}
          value={selectDefaultValue}
          onChange={handleSelectChange}
          helperText={`Please select Bond Interest`}
          margin="normal"
      >
          {entity.map((item, idx) => (
              <MenuItem key={idx} value={item}>
                  {item}
              </MenuItem>
          ))}
      </TextField>
    )
    const inputControl = (
      <TextField
          className={classes.verticalInit}
          label="Coupon Cap"
          value={inputDefaultValue}
          onChange={handleInputChange}
      />)
    return (
      <>
        <FormControlLabel value="Interest" control={<Radio color="primary"/>} label="Bond Interest" />
        <div className={classes.verticalInit}>
          { radioDefaultValue === 'Interest' ? <FormControlLabel control={selectControl}/> : null }
          { selectDefaultValue === 'B' && radioDefaultValue === 'Interest' ? <FormControlLabel control={inputControl}/> : null }
        </div>
      </>
    )
  }

  renderBondPricipal = (setUpdatedProp, index) => (sourceProp, nodeProp, radioDefaultValue) => {
    const { classes } = this.props;
    let selectDefaultValue = _.isUndefined(nodeProp.BondID) ? sourceProp.BondID : nodeProp.BondID
    let switchDefaultValue = _.isUndefined(nodeProp.AmortSchedule) ? sourceProp.AmortSchedule : nodeProp.AmortSchedule
    const handleSelectChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "BondID": e.target.value }})
    const handleSwitchChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "AmortSchedule": e.target.checked }})
    const entity = ['A', 'B', 'SUB']
    selectDefaultValue = selectDefaultValue || ''
    switchDefaultValue = switchDefaultValue || false
    const selectControl = (
      <TextField
          select
          className={classes.verticalInit}
          value={selectDefaultValue}
          onChange={handleSelectChange}
          helperText={`Please select Bond Pricipal`}
          margin="normal"
      >
          {entity.map((item, idx) => (
              <MenuItem key={idx} value={item}>
                  {item}
              </MenuItem>
          ))}
      </TextField>
    )
    const switchControl = (
      <>
        Amort Schedule:
        <Switch 
          color="primary"
          checked={switchDefaultValue}
          onChange={handleSwitchChange}
          />
        </>
    )
    return (
      <>
        <FormControlLabel value="Principal" control={<Radio color="primary"/>} label="Bond Pricipal" />
        <div className={classes.verticalInit}>
          { 
            radioDefaultValue === 'Principal' ? 
            <>
              <FormControlLabel control={selectControl}/>
              <br/>
              { switchControl }
            </> : 
            null 
          }
        </div>
      </>
    )
  }

  renderColPrincipal = (setUpdatedProp, index) => (sourceProp, nodeProp, radioDefaultValue) => {
    const { classes } = this.props;
    let checkboxDefaultValue =  _.isUndefined(nodeProp.CapToABCD) ? sourceProp.CapToABCD : nodeProp.CapToABCD
    const handleCheckboxChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "CapToABCD": e.target.checked }})
    checkboxDefaultValue = checkboxDefaultValue || false
    return (
      <>
        <FormControlLabel value="Principal" control={<Radio color="primary"/>} label="Collateral Principal" />
        <div className={classes.verticalInit}>
          {
            radioDefaultValue === 'Principal' ? 
            <FormControlLabel
              className={classes.verticalInit}
              control={ <Checkbox checked={checkboxDefaultValue} onChange={ handleCheckboxChange } color="primary"/> }
              label="Cap to Collateral Default(a+b+c-d)"
            /> :
              null
          }
        </div>
      </>
    )
  }

  renderFee = (setUpdatedProp, index) => (sourceProp, nodeProp, radioDefaultValue) => {
    const { classes } = this.props;
    let selectDefaultValue = _.isUndefined(nodeProp.FeeID) ? sourceProp.FeeID : nodeProp.FeeID
    let FeeChecked = _.isUndefined(nodeProp.FeeChecked) ? sourceProp.FeeChecked : nodeProp.FeeChecked
    let FeeCap = _.isUndefined(nodeProp.FeeCap) ? sourceProp.FeeCap : nodeProp.FeeCap
    let FeeCapNum = _.isUndefined(nodeProp.FeeCapNum) ? sourceProp.FeeCapNum : nodeProp.FeeCapNum
    let FeeCapPercent = _.isUndefined(nodeProp.FeeCapPercent) ? sourceProp.FeeCapPercent : nodeProp.FeeCapPercent
    const handleSelectChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "FeeID": e.target.value }})
    const handleInputChange = (key, e) => setUpdatedProp({ [index]: { ...nodeProp, [key]: e.target.value }})
    const handleRadioChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "FeeCap": e.target.value }})
    const handleCheckboxChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "FeeChecked": e.target.checked }})
    const entity = ['Tax', 'Servicing Fee', 'Cash Management Fee']
    FeeChecked = FeeChecked || false
    selectDefaultValue = selectDefaultValue || ''
    FeeCap = FeeCap || ''
    FeeCapNum = FeeCapNum || ''
    FeeCapPercent = FeeCapPercent || ''
    const selectControl = (
      <TextField
          select
          className={`${classes.verticalInit} ${classes.select}`}
          value={selectDefaultValue}
          onChange={handleSelectChange}
          helperText={`Please select Fee`}
          margin="normal"
      >
          {entity.map((item, idx) => (
              <MenuItem key={idx} value={item}>
                  {item}
              </MenuItem>
          ))}
      </TextField>
    )
    const radioLabel1 = (
      <TextField
          label="Fee Cap(Num)"
          value={FeeCapNum}
          onChange={handleInputChange.bind(this, 'FeeCapNum')}
      />
    )
    const radioLabel2 = (
      <TextField
          label="Fee Cap(%)"
          value={FeeCapPercent}
          onChange={handleInputChange.bind(this, 'FeeCapPercent')}
      />
    )
    return (
      <>
        <FormControlLabel value="Fee" control={<Radio color="primary"/>} label="Fee" />
        <div className={classes.verticalInit}>
          { 
            radioDefaultValue === 'Fee' ? 
            <>
              <FormControlLabel control={selectControl}/>
              <br/>
              <FormControlLabel
                control={ <Checkbox checked={FeeChecked} onChange={ handleCheckboxChange } color="primary"/> }
                label="Fee Cap"
              />
              <RadioGroup value={FeeCap} onChange = {handleRadioChange}>
                <FormControlLabel value="number" control={<Radio color="primary"/>} label={radioLabel1}/>
                <FormControlLabel value="percent" control={<Radio color="primary"/>} label={radioLabel2}/>
              </RadioGroup>
            </> : 
            null 
          }
        </div>
      </>
    )
  }
  renderAccount = (setUpdatedProp, index) => (sourceProp, nodeProp, radioDefaultValue) => {
    const { classes } = this.props;
    let selectDefaultValue = _.isUndefined(nodeProp.AccountID) ? sourceProp.AccountID : nodeProp.AccountID
    let Capto = _.isUndefined(nodeProp.Capto) ? sourceProp.Capto : nodeProp.Capto
    let CaptoFormula = _.isUndefined(nodeProp.CaptoFormula) ? sourceProp.CaptoFormula : nodeProp.CaptoFormula
    let CaptoNxtPayDue = _.isUndefined(nodeProp.CaptoNxtPayDue) ? sourceProp.CaptoNxtPayDue : nodeProp.CaptoNxtPayDue
    const handleSelectChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "AccountID": e.target.value }})
    const handleInputChange = (key, e) => setUpdatedProp({ [index]: { ...nodeProp, [key]: e.target.value }})
    const handleRadioChange = (e) => setUpdatedProp({ [index]: { ...nodeProp, "Capto": e.target.value }})
    const entity = ['Reserve']
    selectDefaultValue = selectDefaultValue || ''
    Capto = Capto || ''
    CaptoFormula = CaptoFormula || ''
    CaptoNxtPayDue = CaptoNxtPayDue || ''
    const selectControl = (
      <TextField
          select
          className={`${classes.verticalInit} ${classes.select}`}
          value={selectDefaultValue}
          onChange={handleSelectChange}
          helperText={`Please select Fee`}
          margin="normal"
      >
          {entity.map((item, idx) => (
              <MenuItem key={idx} value={item}>
                  {item}
              </MenuItem>
          ))}
      </TextField>
    )
    const radioLabel1 = (
      <TextField
          label="Capto Formula"
          value={CaptoFormula}
          onChange={handleInputChange.bind(this, 'CaptoFormula')}
      />
    )
    const radioLabel2 = (
      <TextField
          label="Capto NxtPay Due"
          value={CaptoNxtPayDue}
          onChange={handleInputChange.bind(this, 'CaptoNxtPayDue')}
      />
    )
    return (
      <>
        <FormControlLabel value="Account" control={<Radio color="primary"/>} label="Account" />
        <div className={classes.verticalInit}>
          { 
            radioDefaultValue === 'Account' ? 
            <>
              <FormControlLabel control={selectControl}/>
              <RadioGroup value={Capto} onChange = {handleRadioChange}>
                <FormControlLabel value="CaptoFormula" control={<Radio color="primary"/>} label={radioLabel1}/>
                <FormControlLabel value="CaptoNxtPayDue" control={<Radio color="primary"/>} label={radioLabel2}/>
              </RadioGroup>
            </> : 
            null 
          }
        </div>
      </>
    )
  }
}

export default ToEntity;