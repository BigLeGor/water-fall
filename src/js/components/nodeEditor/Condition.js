import React, { Component } from 'react';
import _ from 'lodash';
import {
  Switch,
  TextField
} from '@material-ui/core';

class Condition extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { title, selectedNodeProps, nodeProps, setNodeProps } = this.props;
    const { blockProps = {}, updatedNodeProps = {} } = selectedNodeProps;
    const { HasCondition, ConditionalFormula } = blockProps;
    return (
      <React.Fragment>
        { title }
        { this.buildSwitchField(setNodeProps, { ...updatedNodeProps, ...nodeProps })("Has Condition", { HasCondition }) }
        { this.buildObjectTextField(setNodeProps, { ...updatedNodeProps, ...nodeProps })({ ConditionalFormula }) }
      </React.Fragment>
    );
  }

buildSwitchField = (setNodeProps, nodeProps) => (title, sourceProp = {}) => {
  const { classes } = this.props;
  if(_.isUndefined(sourceProp.HasCondition)) {
    return null;
  }
  const [[key, defaultVal = false] = []] = Object.entries(sourceProp);
  const value = _.isUndefined(nodeProps[key]) ? defaultVal : nodeProps[key];
  const onChange = e => setNodeProps({ ...nodeProps, [key]: e.target.checked });
  return (
    <div  className={classes.verticalInit}>
      {title}
      <Switch 
        id={`standard-${key}`}
        color="primary"
        checked={value}
        onChange={onChange}
        />
    </div>
  );
}

buildObjectTextField = (setNodeProps, nodeProps) => (sourceProp = {}) => {
  const [[key, entity] = []] = Object.entries(sourceProp);
  if (!entity) {
      return null;
  }
  const { classes } = this.props;
  const targetProp = nodeProps[key] || {};
  const textFiled = Object.entries(entity).map(([entityKey, defaultVal] = []) => {
    if(_.isNull(defaultVal)) {
      defaultVal = ''
    }
    return(
      <TextField
          id={`standard-${entityKey}`}
          key={`standard-${entityKey}`}
          className={`${classes.textField} ${classes.verticalInit}`}
          label={entityKey}
          value={targetProp[entityKey] || defaultVal}
          onChange={e => setNodeProps({
              ...nodeProps,
              [key]: {
                  ...targetProp,
                  [entityKey]: e.target.value
              }
          })}
          margin="normal"
      />)
    });
    return textFiled;
  }
}

export default Condition;