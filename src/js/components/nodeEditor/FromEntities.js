import React, { Component } from 'react';
import _ from 'lodash';
import {
  FormControl, FormControlLabel,
  Radio, RadioGroup, Checkbox,
} from '@material-ui/core';

class FromEntity extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { title, selectedNodeProps, nodeProps, setNodeProps } = this.props;
    const { blockProps = {}, updatedNodeProps = {} } = selectedNodeProps;
    const { FromEntities } = blockProps;

    if(!FromEntities) {
      return null;
    }
    return (
      <React.Fragment>
        { title }
        { this.buildFromEntitiesEditor(setNodeProps, { ...updatedNodeProps, ...nodeProps })(FromEntities) }
      </React.Fragment>
    );
  }

  buildFromEntitiesEditor = (setNodeProps, nodeProps) => (FromEntities) => {
    const targetProp = nodeProps.FromEntities || {};
    const setUpdatedProp = updatedProp => setNodeProps({
        ...nodeProps,
        FromEntities: [{
            ...targetProp,
            ...updatedProp
        }]
    });
    return this.builRadioField(setUpdatedProp)(FromEntities, targetProp);
  };

  builRadioField = setUpdatedProp => (sourceProp, nodeProp) => {
    const { classes } = this.props;
    const targetProp = nodeProp[0] || {};
    const radioDefaultValue = _.isUndefined(targetProp.CurrentOperator) ? sourceProp[0].CurrentOperator : targetProp.CurrentOperator
    let checkboxDefaultValue = _.isUndefined(targetProp.SupportDeficiency) ? sourceProp[0].SupportDeficiency : targetProp.SupportDeficiency
    const handleRadioChange = (e) => setUpdatedProp({ ...targetProp, "CurrentOperator": e.target.value })
    const handleCheckboxChange = (e) => setUpdatedProp({ ...targetProp, "SupportDeficiency": e.target.checked })
    checkboxDefaultValue = checkboxDefaultValue || false;
    return (
      <FormControl className={classes.verticalInit}>
        <RadioGroup value={radioDefaultValue} onChange = {handleRadioChange}>
          <FormControlLabel value="Interest" control={<Radio color="primary"/>} label="Collateral Interest" />
            { radioDefaultValue === 'Interest' ? 
              <FormControlLabel
                className={classes.verticalInit}
                control={ <Checkbox checked={checkboxDefaultValue} onChange={ handleCheckboxChange } color="primary"/> }
                label="Deficiency Support"
              /> :
               null }
          <FormControlLabel value="Principal" control={<Radio color="primary"/>} label="Collateral Principal" />
          <FormControlLabel value="Cash" control={<Radio color="primary"/>} label="Collateral Cash" />
        </RadioGroup>
      </FormControl>
    )
  };
}

export default FromEntity;