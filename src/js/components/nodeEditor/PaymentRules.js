import React, { Component } from 'react';
import _ from 'lodash';
import {
  FormControl, FormControlLabel,
  Radio, RadioGroup,
  TextField,
} from '@material-ui/core';

class PaymentRules extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { title, selectedNodeProps, nodeProps, setNodeProps } = this.props;
    const { blockProps = {}, updatedNodeProps = {} } = selectedNodeProps;

    return (
      <React.Fragment>
        { title }
        { this.buildPaymentRules(setNodeProps, { ...updatedNodeProps, ...nodeProps })(blockProps) }
      </React.Fragment>
    );
  }

  buildPaymentRules = (setNodeProps, nodeProps) => (blockProps) => {
    const { Priority, AmoutFormula } = blockProps;
    return (
      <>
        { this.builRadioField(setNodeProps)({ Priority }, nodeProps) }
        { this.buildObjectTextField(setNodeProps)({ AmoutFormula }, nodeProps) }
      </>
    )
  };

  builRadioField = setNodeProps => (sourceProp, nodeProp) => {
    if( _.isUndefined(sourceProp.Priority)) {
      return null;
    }
    const { classes } = this.props;
    const [[key, value] = []] = Object.entries(sourceProp) 
    const radioDefaultValue = _.isUndefined(nodeProp[key]) ? value :nodeProp[key]
    const handleRadioChange = (e) => setNodeProps({ ...nodeProp, [key]: e.target.value })
    return (
      <FormControl className={classes.verticalInit}>
        <RadioGroup value={radioDefaultValue} onChange = {handleRadioChange}>
          <FormControlLabel key="SEQUENTIAL" value="SEQUENTIAL" control={<Radio color="primary"/>} label="Sequential" />
          <FormControlLabel key="PRORATA" value="PRORATA" control={<Radio color="primary"/>} label="ProRata" />
        </RadioGroup>
      </FormControl>
    )
  };

  buildObjectTextField = (setNodeProps) => (sourceProp = {}, nodeProps) => {
    const [[key, entity] = []] = Object.entries(sourceProp);
    if (!entity) {
        return null;
    }
    const { classes } = this.props;
    const targetProp = nodeProps[key] || {};
    const textFiled = Object.entries(entity).map(([entityKey, defaultVal] = []) => {
      if(_.isNull(defaultVal)) {
        defaultVal = ''
      }
        return (
        <TextField
            id={`standard-${entityKey}`}
            key={`standard-${entityKey}`}
            className={`${classes.textField} ${classes.verticalInit}`}
            label={entityKey}
            value={targetProp[entityKey] || defaultVal}
            onChange={e => setNodeProps({
                ...nodeProps,
                [key]: {
                    ...targetProp,
                    [entityKey]: e.target.value
                }
            })}
            margin="normal"
        />)
      });
      return textFiled;
    }
}

export default PaymentRules;