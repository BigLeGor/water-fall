import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import WaterfallDiagram from './container/WaterfallDiagram';
import NodePropsEditor from './container/NodePropsEditor';
import { loadNodeTemplateMap } from '../js/actions/palette';

import '../css/App.css';

const mapDispatchToProps = dispatch => ({
    init: () => dispatch(loadNodeTemplateMap())
});

const App = ({ init, ...props }) => {
    useEffect(() => {
        init();
    }, []);

    return (
        <div className="App">
            <WaterfallDiagram {...props} />
            <NodePropsEditor />
        </div>
    );
};

export default connect(null, mapDispatchToProps)(App);