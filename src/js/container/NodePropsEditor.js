import React from 'react';
import { connect } from 'react-redux';
import NodePropsEditor from '../components/NodePropsEditor';
import { getSelectedNodeKeys } from '../stores/selectedNodeKeys';
import { getNodePropsState } from '../stores/updatedNodeProps';
import { getPaletteBlockPropsState } from '../stores/paletteModel';
import { updateTargetNodeProps, nodeDeselected } from '../actions';

const mapStateToProps = state => {
    const selectedNodes = getSelectedNodeKeys(state);
    const [[selectedNodeKey, selectedNodeId] = []] = Object.entries(selectedNodes);
    const updatedNodeProps = selectedNodeKey && getNodePropsState(state, selectedNodeKey);
    const blockProps = getPaletteBlockPropsState(state)[selectedNodeId];

    return {
        selectedNodeKey,
        selectedNodeProps: {
            blockProps,
            updatedNodeProps
        }
    };
};

const mapDispatchToProps = dispatch => ({
    onNodePropUpdate: (key, updatedProps) => {
        updatedProps && dispatch(updateTargetNodeProps(key, updatedProps));
    },
    buildOnClose: selectedNodeKey => () => dispatch(nodeDeselected(selectedNodeKey))
});

const NodePropsEditorContainer = ({ selectedNodeKey, selectedNodeProps, onNodePropUpdate, buildOnClose }) => {
    const onClose = buildOnClose(selectedNodeKey);
    return (
        <NodePropsEditor
            selectedNodeKey={selectedNodeKey}
            selectedNodeProps={selectedNodeProps}
            onNodePropUpdate={onNodePropUpdate}
            onClose={onClose}
        />
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(NodePropsEditorContainer);
