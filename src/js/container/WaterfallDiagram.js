import React from 'react';
import { ModelChangeEventType } from 'react-gojs';
import { connect } from 'react-redux';
import shortid from 'shortid';
import WaterfallDiagram from '../components/WaterfallDiagram';
import WaterfallPalette from '../components/WaterfallPalette';
import PaletteList from './WaterfallPalette';
import { getDiagramModelState } from '../stores/diagramModel';
import { getPaletteNodeTplState } from '../stores/paletteModel';
import {
    addNode,
    addLink,
    nodeDeselected,
    nodeSelected,
    updateTargetNodeProps,
    removeNode,
    removeLink
} from '../actions';

const mapStateToProps = (state) => ({
    model: getDiagramModelState(state),
    paletteModel: getPaletteNodeTplState(state)
});

const mapDispatchToProps = dispatch => ({
    onNodeClick: (key, blockPropsId, isSelected) => {
        if (isSelected) {
            //dispatch(updateTargetNodeProps(key, blockPropsId, { test: "lig" }));
            dispatch(nodeSelected({ [key]: blockPropsId }));
        }
    },
    onNodeSelection: (key, isSelected) => !isSelected && dispatch(nodeDeselected(key)),
    onModelChange: event => {
        const eventHandles = Object.freeze({
            [ModelChangeEventType.Remove]: ({ nodeData, linkData }) => {
                if (nodeData) {
                    dispatch(removeNode(nodeData.key));
                }
                if (linkData) {
                    dispatch(removeLink(linkData));
                }
            },
            [ModelChangeEventType.Add]: ({ nodeData, linkData }) => {
                if (nodeData) {
                    dispatch(addNode(nodeData));
                }
                if (linkData) {
                    dispatch(addLink(linkData));
                }
            }
        });
        eventHandles[event.eventType](event, dispatch);
    },
    setPaletteNodeTemplate: nodeTpl => dispatch(setPaletteNodeTemplate(nodeTpl))
});

const intergratedPalette = "palette-integrated";
const customPalette = "palette-custom";

const WaterfallDiagramContainer = React.memo(({
    model,
    paletteModel,
    onNodeClick,
    onNodeSelection,
    onModelChange,
    onTextChange
}) => ([
    /*<PaletteList key={shortid.generate()} >
        <WaterfallPalette
            paletteId={intergratedPalette}
        />
        <WaterfallPalette
            paletteId={customPalette}
        />
    </PaletteList>, */
    <div key={shortid.generate()} className="palettes">
        <WaterfallPalette
            paletteId={intergratedPalette}
        />
        <WaterfallPalette
            paletteId={customPalette}
        />
    </div>,
    <WaterfallDiagram
        key={shortid.generate()}
        model={model}
        onNodeClick={onNodeClick}
        onNodeSelection={onNodeSelection}
        onModelChange={onModelChange}
        onTextChange={onTextChange}
        createPalette={
            WaterfallPalette.createPalette(
                paletteModel
            )(
                {
                    paletteId: intergratedPalette,
                    targetBlockType: "INTELLIGENT"
                },
                {
                    paletteId: customPalette,
                    targetBlockType: "CUSTOM"
                }
            )
        }
    />
]));

export default connect(mapStateToProps, mapDispatchToProps)(WaterfallDiagramContainer);