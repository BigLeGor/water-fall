import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import shortid from 'shortid';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));

const buildPaletteItem = (paletteItem, idx) => (itemOpen, handleClick) => ([
    <ListItem button onClick={handleClick} key={shortid.generate()} >
        <ListItemIcon>
            <InboxIcon />
        </ListItemIcon>
        <ListItemText primary={`Item-${idx}`} />
        {itemOpen ? <ExpandLess /> : <ExpandMore />}
    </ListItem>,
    <Collapse in={itemOpen} timeout="auto" unmountOnExit key={shortid.generate()} >
        {paletteItem}
    </Collapse>
]);

const buildListItems = (paletteItems, itemsOpenState) => handleClick =>
    paletteItems.map((paletteItem, idx) => {
        const itemOpenState = itemsOpenState[idx];
        const itemClick = () => handleClick(idx)(!itemOpenState);
        return buildPaletteItem(paletteItem, idx)(itemOpenState, itemClick);
    });

const PaletteList = ({ children: paletteItems }) => {
    const classes = useStyles();
    const [itemsOpenState, setOpen] = React.useState([true, true]);

    const handleClick = idx => openState => {
        itemsOpenState[idx] = openState
        setOpen([...itemsOpenState]);
    }

    if (!paletteItems) {
        return null;
    }

    return (
        <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
                <ListSubheader component="div" className="palettes">
                    Waterfall Palettes
                </ListSubheader>
            }
            className={classes.root}
        >
            {buildListItems(paletteItems, itemsOpenState)(handleClick)}
        </List>
    );
};

export default PaletteList;