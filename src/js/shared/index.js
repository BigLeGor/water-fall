import { save as fileSave } from 'save-file';
import { readAsText } from 'promisify-file-reader';

export * from  './waterfall';

export const saveFile = async (jsonObj) => {
    const fileName = "water-fall.json"
    await fileSave(jsonObj, fileName);
};

export const loadFile = async (e) => {
    const [targetFile] = e.target.files;
    if (targetFile) {
        const jsonString = await readAsText(targetFile);
        return JSON.parse(jsonString);
    }
    return null;
};

// print the diagram by opening a new window holding SVG images of the diagram contents for each page
export const printDiagram = (myDiagram) => {
    const svgWindow = window.open();
    if (!svgWindow) return;  // failure to open a new Window
    const printSize = new go.Size(700, 960);
    const bnds = myDiagram.documentBounds;
    const x = bnds.x;
    const y = bnds.y;
    while (y < bnds.bottom) {
        while (x < bnds.right) {
            const svg = myDiagram.makeSVG({ scale: 1.0, position: new go.Point(x, y), size: printSize });
            svgWindow.document.body.appendChild(svg);
            x += printSize.width;
        }
        x = bnds.x;
        y += printSize.height;
    }
    setTimeout(function () { svgWindow.print(); }, 1);
};


const colors = ['lightblue', 'orange', 'lightgreen', 'pink', 'yellow', 'red', 'grey', 'magenta', 'cyan'];

export const getRandomColor = () => colors[Math.floor(Math.random() * colors.length)];

export const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)));

export const paletteNodeColor = Object.freeze({
    Start: 'lightgreen',
    Conditions: '#ea9999',
    Executions: '#99ccff',
    Assigns: '#f9cb9c'
});