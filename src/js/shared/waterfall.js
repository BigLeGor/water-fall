const buildNodeDataArray = (waterfallArray, initLinkDataArray) => {
    if (!Array.isArray(waterfallArray)) {
        return [];
    }

    const initAcc = {
        nodeDataArray: [
            { key: -1, category: "Start", label: "Start" }
        ],
        linkDataArray: initLinkDataArray || [
            { from: -1, to: 0 }
        ]
    };

    return waterfallArray.reduce(
        (acc, curr) => {
            const { nodeDataArray, linkDataArray } = acc;
            const { ID, Label: label, BlockType: blockType, Category: category, ChildSteps, ...props } = curr;
            const node = {
                key: ID,
                category,
                label,
                blockType
            };
            acc.nodeDataArray = [...nodeDataArray, node];

            if (ChildSteps) {
                acc.linkDataArray = [
                    ...linkDataArray,
                    ...ChildSteps.map(step => ({
                        from: ID,
                        to: step
                    }))
                ];
            }

            return acc;
        },
        initAcc);
};

export const buildGraphLinksModel = (waterfallArray, initLinkDataArray) => {
    const model = {
        "nodeDataArray": [],
        "linkDataArray": []
    };

    ({
        nodeDataArray: model.nodeDataArray,
        linkDataArray: model.linkDataArray
    } = buildNodeDataArray(waterfallArray, initLinkDataArray));

    return model;
};