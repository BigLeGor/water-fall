import go from "gojs";

class Inspector {
  static showIfNode(part) { 
    return part instanceof go.Node 
  }

  static showIfLink(part) {
    return part instanceof go.Link
  }

  static showIfGroup(part) {
    return part instanceof go.Group
  };

  static showIfPresent(data, propname) {
    if (data instanceof go.Part) data = data.data;
    return typeof data === "object" && data[propname] !== undefined;
  };

  constructor(divid, diagram, options) {
    const mainDiv = document.getElementById(divid);
    mainDiv.className = "inspector";
    mainDiv.innerHTML = "";
    this._div = mainDiv;
    this._diagram = diagram;
    this._inspectedProperties = {};
    this._multipleProperties = {};
  
    // Either a GoJS Part or a simple data object, such as Model.modelData
    this.inspectedObject = null;
  
    // Inspector options defaults:
    this.includesOwnProperties = true;
    this.declaredProperties = {};
    this.inspectsSelection = true;
    this.propertyModified = null;
    this.multipleSelection = false;
    this.showAllProperties = false;
    this.showSize = 0;
  
    if (options !== undefined) {
      if (options["includesOwnProperties"] !== undefined) this.includesOwnProperties = options["includesOwnProperties"];
      if (options["properties"] !== undefined) this.declaredProperties = options["properties"];
      if (options["inspectSelection"] !== undefined) this.inspectsSelection = options["inspectSelection"];
      if (options["propertyModified"] !== undefined) this.propertyModified = options["propertyModified"];
      if (options['multipleSelection'] !== undefined) this.multipleSelection = options['multipleSelection'];
      if (options['showAllProperties'] !== undefined) this.showAllProperties = options['showAllProperties'];
      if (options['showSize'] !== undefined) this.showSize = options['showSize'];
    }
  
    diagram.addModelChangedListener(function(e) {
      if (e.isTransactionFinished) this.inspectObject();
    });
    if (this.inspectsSelection) {
      diagram.addDiagramListener("ChangedSelection", function(e) { self.inspectObject(); });
    }
  }

  inspectObject(object) {
    let inspectedObject = null;
    let inspectedObjects = null;
    if (object === null) return;
    if (object === undefined) {
      if (this.inspectsSelection) {
        if (this.multipleSelection) { // gets the selection if multiple selection is true
          inspectedObjects = this._diagram.selection;
        } else { // otherwise grab the first object
          inspectedObject = this._diagram.selection.first();
        }
      } else { // if there is a single inspected object
        inspectedObject = this.inspectedObject;
      }
    } else { // if object was passed in as a parameter
      inspectedObject = object;
    }
    if (inspectedObjects && inspectedObjects.count === 1) {
      inspectedObject = inspectedObjects.first();
    }
    if (inspectedObjects && inspectedObjects.count <= 1) {
      inspectedObjects = null;
    }
  
    // single object or no objects
    if (!inspectedObjects || !this.multipleSelection) {
      if (inspectedObject === null) {
        this.inspectedObject = inspectedObject;
        this.updateAllHTML();
        return;
      }
  
      this.inspectedObject = inspectedObject;
      if (this.inspectObject === null) return;
      const mainDiv = this._div;
      mainDiv.innerHTML = '';
  
      // use either the Part.data or the object itself (for model.modelData)
      const data = (inspectedObject instanceof go.Part) ? inspectedObject.data : inspectedObject;
      if (!data) return;
      // Build table:
      const table = document.createElement('table');
      const tbody = document.createElement('tbody');
      this._inspectedProperties = {};
      this.tabIndex = 0;
      const declaredProperties = this.declaredProperties;
  
      // Go through all the properties passed in to the inspector and show them, if appropriate:
      for (let name in declaredProperties) {
        const desc = declaredProperties[name];
        if (!this.canShowProperty(name, desc, inspectedObject)) continue;
        const val = this.findValue(name, desc, data);
        tbody.appendChild(this.buildPropertyRow(name, val));
      }
      // Go through all the properties on the model data and show them, if appropriate:
      if (this.includesOwnProperties) {
        for (let k in data) {
          if (k === '__gohashid') continue; // skip internal GoJS hash property
          if (this._inspectedProperties[k]) continue; // already exists
          if (declaredProperties[k] && !this.canShowProperty(k, declaredProperties[k], inspectedObject)) continue;
          tbody.appendChild(this.buildPropertyRow(k, data[k]));
        }
      }
  
      table.appendChild(tbody);
      mainDiv.appendChild(table);
    } else { // multiple objects selected
      const mainDiv = this._div;
      mainDiv.innerHTML = '';
      const shared = new go.Map(); // for properties that the nodes have in common
      const properties = new go.Map(); // for adding properties
      const all = new go.Map(); // used later to prevent changing properties when unneeded
      const it = inspectedObjects.iterator;
      // Build table:
      const table = document.createElement('table');
      const tbody = document.createElement('tbody');
      this._inspectedProperties = {};
      this.tabIndex = 0;
      const declaredProperties = this.declaredProperties;
      it.next();
      inspectedObject = it.value;
      this.inspectedObject = inspectedObject;
      let data = (inspectedObject instanceof go.Part) ? inspectedObject.data : inspectedObject;
      if (data) { // initial pass to set shared and all
        // Go through all the properties passed in to the inspector and add them to the map, if appropriate:
        for (let name in declaredProperties) {
          const desc = declaredProperties[name];
          if (!this.canShowProperty(name, desc, inspectedObject)) continue;
          const val = this.findValue(name, desc, data);
          if (val === '' && desc && desc.type === 'checkbox') {
            shared.add(name, false);
            all.add(name, false);
          } else {
            shared.add(name, val);
            all.add(name, val);
          }
        }
        // Go through all the properties on the model data and add them to the map, if appropriate:
        if (this.includesOwnProperties) {
          for (let k in data) {
            if (k === '__gohashid') continue; // skip internal GoJS hash property
            if (this._inspectedProperties[k]) continue; // already exists
            if (declaredProperties[k] && !this.canShowProperty(k, declaredProperties[k], inspectedObject)) continue;
            shared.add(k, data[k]);
            all.add(k, data[k]);
          }
        }
      }
      let nodeCount = 2;
      while (it.next() && (this.showSize < 1 || nodeCount <= this.showSize)) { // grabs all the properties from the other selected objects
        properties.clear();
        inspectedObject = it.value;
        if (inspectedObject) {
          // use either the Part.data or the object itself (for model.modelData)
          data = (inspectedObject instanceof go.Part) ? inspectedObject.data : inspectedObject;
          if (data) {
            // Go through all the properties passed in to the inspector and add them to properties to add, if appropriate:
            for (let name in declaredProperties) {
              const desc = declaredProperties[name];
              if (!this.canShowProperty(name, desc, inspectedObject)) continue;
              const val = this.findValue(name, desc, data);
              if (val === '' && desc && desc.type === 'checkbox') {
                properties.add(name, false);
              } else {
                properties.add(name, val);
              }
            }
            // Go through all the properties on the model data and add them to properties to add, if appropriate:
            if (this.includesOwnProperties) {
              for (let k in data) {
                if (k === '__gohashid') continue; // skip internal GoJS hash property
                if (this._inspectedProperties[k]) continue; // already exists
                if (declaredProperties[k] && !this.canShowProperty(k, declaredProperties[k], inspectedObject)) continue;
                properties.add(k, data[k]);
              }
            }
          }
        }
        if (!this.showAllProperties) {
          // Cleans up shared map with properties that aren't shared between the selected objects
          // Also adds properties to the add and shared maps if applicable
          const addIt = shared.iterator;
          const toRemove = [];
          while (addIt.next()) {
            if (properties.has(addIt.key)) {
              const newVal = all.get(addIt.key) + '|' + properties.get(addIt.key);
              all.set(addIt.key, newVal);
              if ((declaredProperties[addIt.key] && declaredProperties[addIt.key].type !== 'color'
                && declaredProperties[addIt.key].type !== 'checkbox' && declaredProperties[addIt.key].type !== 'select')
                || !declaredProperties[addIt.key]) { // for non-string properties i.e color
                newVal = `${shared.get(addIt.key)}|${properties.get(addIt.key)}`;
                shared.set(addIt.key, newVal);
              }
            } else { // toRemove array since addIt is still iterating
              toRemove.push(addIt.key);
            }
          }
          for (let i = 0; i < toRemove.length; i++) { // removes anything that doesn't showAllPropertiess
            shared.remove(toRemove[i]);
            all.remove(toRemove[i]);
          }
        } else {
          // Adds missing properties to all with the correct amount of seperators
          let addIt = properties.iterator;
          while (addIt.next()) {
            if (all.has(addIt.key)) {
              if ((declaredProperties[addIt.key] && declaredProperties[addIt.key].type !== 'color'
                && declaredProperties[addIt.key].type !== 'checkbox' && declaredProperties[addIt.key].type !== 'select')
                || !declaredProperties[addIt.key]) { // for non-string properties i.e color
                let newVal = `${all.get(addIt.key)}|${properties.get(addIt.key)}`;
                all.set(addIt.key, newVal);
              }
            } else {
              let newVal = '';
              for (let i = 0; i < nodeCount - 1; i++) {
                newVal = `${newVal}|`
              }
              newVal = `${newVal}${properties.get(addIt.key)}`;
              all.set(addIt.key, newVal);
            }
          }
          // Adds bars in case properties is not in all
          addIt = all.iterator;
          while (addIt.next()) {
            if (!properties.has(addIt.key)) {
              if ((declaredProperties[addIt.key] && declaredProperties[addIt.key].type !== 'color'
                && declaredProperties[addIt.key].type !== 'checkbox' && declaredProperties[addIt.key].type !== 'select')
                || !declaredProperties[addIt.key]) { // for non-string properties i.e color
                var newVal = all.get(addIt.key) + '|';
                all.set(addIt.key, newVal);
              }
            }
          }
        }
        nodeCount++;
      }
      // builds the table property rows and sets multipleProperties to help with updateall
      let mapIt;
      if (!this.showAllProperties) {
        mapIt = shared.iterator;
      } else {
        mapIt = all.iterator;
      }
      while (mapIt.next()) {
        tbody.appendChild(this.buildPropertyRow(mapIt.key, mapIt.value)); // shows the properties that are allowed
      }
      table.appendChild(tbody);
      mainDiv.appendChild(table);
      const allIt = all.iterator;
      while (allIt.next()) {
        this._multipleProperties[allIt.key] = allIt.value; // used for updateall to know which properties to change
      }
    }
  }

  canShowProperty(propertyName, propertyDesc, inspectedObject) {
    if (propertyDesc.show === false) {
      return false;
    }
    // if "show" is a predicate, make sure it passes or do not show this property
    if (typeof propertyDesc.show === "function") {
      return propertyDesc.show(inspectedObject, propertyName);
    }

    return true;
  }

  canEditProperty(propertyName, propertyDesc, inspectedObject) {
    if (this._diagram.isReadOnly || this._diagram.isModelReadOnly) {
      return false;
    }
    // assume property values that are functions of Objects cannot be edited
    const data = (inspectedObject instanceof go.Part) ? inspectedObject.data : inspectedObject;
    const valType = typeof data[propertyName];
    if (valType === "function") {
      return false;
    }
    if (propertyDesc) {
      if (propertyDesc.readOnly === true) {
        return false;
      }
      // if "readOnly" is a predicate, make sure it passes or do not show this property
      if (typeof propertyDesc.readOnly === "function") {
        return !propertyDesc.readOnly(inspectedObject, propertyName);
      }
    }
    return true;
  }

  findValue(propName, propDesc, data) {
    let val = '';
    if (propDesc && propDesc.defaultValue !== undefined) {
      val = propDesc.defaultValue;
    }
    if (data[propName] !== undefined) { 
      val = data[propName];
    }
    if (val === undefined) { 
      return '';
    }

    return val;
  }

  buildPropertyRow(propertyName, propertyValue) {
    const mainDiv = this._div;
    const tr = document.createElement("tr");
  
    const td1 = document.createElement("td");
    td1.textContent = propertyName;
    tr.appendChild(td1);
  
    const td2 = document.createElement("td");
    const decProp = this.declaredProperties[propertyName];
    let input = null;
    const updateAll = () => this.updateAllProperties();
  
    if (decProp && decProp.type === "select") {
      input = document.createElement("select");
      this.updateSelect(decProp, input, propertyName, propertyValue);
      input.addEventListener("change", updateAll);
    } else {
      input = document.createElement("input");
      input.value = this.convertToString(propertyValue);

      if (decProp) {
        const t = decProp.type;
        if (t !== 'string' && t !== 'number' && t !== 'boolean' &&
          t !== 'arrayofnumber' && t !== 'point' && t !== 'size' &&
          t !== 'rect' && t !== 'spot' && t !== 'margin') {
          input.setAttribute("type", decProp.type);
        }
        if (decProp.type === "color") {
          if (input.type === "color") {
            input.value = this.convertToColor(propertyValue);
            // input.addEventListener("input", updateall);
            input.addEventListener("change", updateAll);
          }
        } if (decProp.type === "checkbox") {
          input.checked = !!propertyValue;
          input.addEventListener("change", updateAll);
        }
      }
      if (input.type !== "color") {
        input.addEventListener("blur", updateAll);
      }
    }
  
    if (input) {
      input.tabIndex = this.tabIndex++;
      input.disabled = !this.canEditProperty(propertyName, decProp, this.inspectedObject);
      td2.appendChild(input);
    }
    tr.appendChild(td2);
  
    this._inspectedProperties[propertyName] = input;
    return tr;
  }

  convertToColor(propertyValue) {
    const ctx = document.createElement("canvas").getContext("2d");
    ctx.fillStyle = propertyValue;
    return ctx.fillStyle;
  }

  convertToArrayOfNumber(propertyValue) {
    if (propertyValue === "null") {
      return null;
    }
    const split = propertyValue.split(' ');
    const arr = [];
    for (let i = 0; i < split.length; i++) {
      const str = split[i];
      if (!str) continue;
      arr.push(parseFloat(str));
    }
    return arr;
  }

  convertToString(x) {
    if (x === undefined) {
      return "undefined";
    }
    if (x === null) {
      return "null";
    }
    if (x instanceof go.Point) {
      return go.Point.stringify(x);
    }
    if (x instanceof go.Size) {
      return go.Size.stringify(x);
    }
    if (x instanceof go.Rect) {
      return go.Rect.stringify(x);
    }
    if (x instanceof go.Spot) {
      return go.Spot.stringify(x);
    }
    if (x instanceof go.Margin) {
      return go.Margin.stringify(x);
    }
    if (x instanceof go.List) {
      return this.convertToString(x.toArray());
    }
    if (Array.isArray(x)) {
      let str = "";
      for (let i = 0; i < x.length; i++) {
        if (i > 0) {
          str = `${str} `;
        }
        str = `${str}${this.convertToString(x[i])}`;
      }
      return str;
    }
    return x.toString();
  }

  updateAllHTML() {
    const inspectedProps = this._inspectedProperties;
    const diagram = this._diagram;
    const isPart = this.inspectedObject instanceof go.Part;
    const data = isPart ? this.inspectedObject.data : this.inspectedObject;
    if (!data) {  // clear out all of the fields
      for (let name in inspectedProps) {
        const input = inspectedProps[name];
        if (input instanceof HTMLSelectElement) {
          input.innerHTML = "";
        } else if (input.type === "color") {
          input.value = "#000000";
        } else if (input.type === "checkbox") {
          input.checked = false;
        } else {
          input.value = "";
        }
      }
    } else {
      for (let name in inspectedProps) {
        const input = inspectedProps[name];
        const propertyValue = data[name];
        if (input instanceof HTMLSelectElement) {
          const decProp = this.declaredProperties[name];
          this.updateSelect(decProp, input, name, propertyValue);
        } else if (input.type === "color") {
          input.value = this.convertToColor(propertyValue);
        } else if (input.type === "checkbox") {
          input.checked = !!propertyValue;
        } else {
          input.value = this.convertToString(propertyValue);
        }
      }
    }
  }
  
  updateSelect(decProp, select, propertyName, propertyValue) {
    select.innerHTML = "";  // clear out anything that was there
    const choices = decProp.choices;
    if (typeof choices === "function") {
      choices = choices(this.inspectedObject, propertyName);
    }
    if (!Array.isArray(choices)) {
      choices = [];
    }
    decProp.choicesArray = choices;  // remember list of actual choice values (not strings)
    for (let i = 0; i < choices.length; i++) {
      const choice = choices[i];
      const opt = document.createElement("option");
      opt.text = this.convertToString(choice);
      select.add(opt, null);
    }
    select.value = this.convertToString(propertyValue);
  }

  updateAllProperties() {
    const inspectedProps = this._inspectedProperties;
    const diagram = this._diagram;
    if (diagram.selection.count === 1 || !this.multipleSelection) { // single object update
      const isPart = this.inspectedObject instanceof go.Part;
      const data = isPart ? this.inspectedObject.data : this.inspectedObject;
      if (!data) {
        return;  // must not try to update data when there's no data!
      }
  
      diagram.startTransaction('set all properties');
      for (let name in inspectedProps) {
        const input = inspectedProps[name];
        let value = input.value;
  
        // don't update "readOnly" data properties
        const decProp = this.declaredProperties[name];
        if (!this.canEditProperty(name, decProp, this.inspectedObject)) {
          continue;
        }
  
        // If it's a boolean, or if its previous value was boolean,
        // parse the value to be a boolean and then update the input.value to match
        let type = '';
        if (decProp !== undefined && decProp.type !== undefined) {
          type = decProp.type;
        }
        if (type === '') {
          const oldVal = data[name];
          if (typeof oldVal === 'boolean') {
            type = 'boolean'; // infer boolean
          } else if (typeof oldVal === 'number') {
            type = 'number';
          } else if (oldVal instanceof go.Point) {
            type = 'point';
          } else if (oldVal instanceof go.Size) {
            type = 'size';
          } else if (oldVal instanceof go.Rect) {
            type = 'rect';
          } else if (oldVal instanceof go.Spot) {
            type = 'spot';
          } else if (oldVal instanceof go.Margin) {
            type = 'margin';
          }
        }
  
        // convert to specific type, if needed
        switch (type) {
          case 'boolean': value = !(value === false || value === 'false' || value === '0'); break;
          case 'number': value = parseFloat(value); break;
          case 'arrayofnumber': value = this.convertToArrayOfNumber(value); break;
          case 'point': value = go.Point.parse(value); break;
          case 'size': value = go.Size.parse(value); break;
          case 'rect': value = go.Rect.parse(value); break;
          case 'spot': value = go.Spot.parse(value); break;
          case 'margin': value = go.Margin.parse(value); break;
          case 'checkbox': value = input.checked; break;
          case 'select': value = decProp.choicesArray[input.selectedIndex]; break;
        }
  
        // in case parsed to be different, such as in the case of boolean values,
        // the value shown should match the actual value
        input.value = value;
  
        // modify the data object in an undo-able fashion
        diagram.model.setDataProperty(data, name, value);
  
        // notify any listener
        if (this.propertyModified !== null) {
          this.propertyModified(name, value, this);
        }
      }
      diagram.commitTransaction('set all properties');
    } else { // selection object update
      diagram.startTransaction('set all properties');
      for (let name in inspectedProps) {
        let input = inspectedProps[name];
        let value = input.value;
        const arr1 = value.split('|');
        let arr2 = [];
        if (this._multipleProperties[name]) {
          // don't split if it is union and its checkbox type
          if (this.declaredProperties[name] && this.declaredProperties[name].type === 'checkbox' && this.showAllProperties) {
            arr2.push(this._multipleProperties[name]);
          } else {
            arr2 = this._multipleProperties[name].toString().split('|');
          }
        }
        const it = diagram.selection.iterator;
        let change = false;
        if (this.declaredProperties[name] && this.declaredProperties[name].type === 'checkbox') {
          change = true; // always change checkbox
        }
        if (arr1.length < arr2.length // i.e Alpha|Beta -> Alpha procs the change
          && (!this.declaredProperties[name] // from and to links
            || !(this.declaredProperties[name] // do not change color checkbox and choices due to them always having less
              && (this.declaredProperties[name].type === 'color' || this.declaredProperties[name].type === 'checkbox' || this.declaredProperties[name].type === 'choices')))) {
          change = true;
        } else { // standard detection in change in properties
          for (let j = 0; j < arr1.length && j < arr2.length; j++) {
            if (!(arr1[j] === arr2[j])
              && !(this.declaredProperties[name] && this.declaredProperties[name].type === 'color' && arr1[j].toLowerCase() === arr2[j].toLowerCase())) {
              change = true;
            }
          }
        }
        if (change) { // only change properties it needs to change instead all of them
          for (let i = 0; i < diagram.selection.count; i++) {
            it.next();
            const isPart = it.value instanceof go.Part;
            const data = isPart ? it.value.data : it.value;
  
            if (data) { // ignores the selected node if there is no data
              if (i < arr1.length) {
                value = arr1[i];
              } else {
                value = arr1[0];
              }
  
              // don't update "readOnly" data properties
              const decProp = this.declaredProperties[name];
              if (!this.canEditProperty(name, decProp, it.value)) {
                continue;
              }
  
              // If it's a boolean, or if its previous value was boolean,
              // parse the value to be a boolean and then update the input.value to match
              let type = '';
              if (decProp !== undefined && decProp.type !== undefined) {
                type = decProp.type;
              }
              if (type === '') {
                let oldVal = data[name];
                if (typeof oldVal === 'boolean') {
                  type = 'boolean'; // infer boolean
                } else if (typeof oldVal === 'number') {
                  type = 'number';
                } else if (oldVal instanceof go.Point) {
                  type = 'point';
                } else if (oldVal instanceof go.Size) {
                  type = 'size';
                } else if (oldVal instanceof go.Rect) {
                  type = 'rect';
                } else if (oldVal instanceof go.Spot) {
                  type = 'spot';
                } else if (oldVal instanceof go.Margin) {
                  type = 'margin';
                }
              }
  
              // convert to specific type, if needed
              switch (type) {
                case 'boolean': value = !(value === false || value === 'false' || value === '0'); break;
                case 'number': value = parseFloat(value); break;
                case 'arrayofnumber': value = this.convertToArrayOfNumber(value); break;
                case 'point': value = go.Point.parse(value); break;
                case 'size': value = go.Size.parse(value); break;
                case 'rect': value = go.Rect.parse(value); break;
                case 'spot': value = go.Spot.parse(value); break;
                case 'margin': value = go.Margin.parse(value); break;
                case 'checkbox': value = input.checked; break;
                case 'select': value = decProp.choicesArray[input.selectedIndex]; break;
              }
  
              // in case parsed to be different, such as in the case of boolean values,
              // the value shown should match the actual value
              input.value = value;
  
              // modify the data object in an undo-able fashion
              diagram.model.setDataProperty(data, name, value);
  
              // notify any listener
              if (this.propertyModified !== null) {
                this.propertyModified(name, value, this);
              }
            }
          }
        }
      }
      diagram.commitTransaction('set all properties');
    }
  }
}

export default Inspector;