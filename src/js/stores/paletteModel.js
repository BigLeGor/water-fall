import { namespaceConfig, staticPropertyConfig } from 'fast-redux'

const namespace = 'paletteModel';

const DEFAULT_STATE = {
    nodeTemplateMap: [],
    blockProps: {}
};

export const {
    action: paletteModelAction,
} = namespaceConfig(namespace, DEFAULT_STATE);

const {
    propertyAction: paletteNodeTplAction,
    getPropertyState: getPaletteNodeTplState
} = staticPropertyConfig(paletteModelAction, 'nodeTemplateMap');

const {
    propertyAction: paletteBlockPropsAction,
    getPropertyState: getPaletteBlockPropsState
} = staticPropertyConfig(paletteModelAction, 'blockProps');

export {
    DEFAULT_STATE as paletteModel,
    paletteNodeTplAction,
    getPaletteNodeTplState,
    paletteBlockPropsAction,
    getPaletteBlockPropsState
};