import { namespaceConfig, dynamicPropertyConfig } from 'fast-redux'

const DEFAULT_STATE = {};

const {
    action
} = namespaceConfig('updatedNodeKeys', DEFAULT_STATE);

const {
    propertyAction: nodePropsAction,
    getPropertyState: getNodePropsState
} = dynamicPropertyConfig(action, DEFAULT_STATE);

export {
    nodePropsAction,
    getNodePropsState
};