import { namespaceConfig, staticPropertyConfig } from 'fast-redux';
import { paletteNodeColor } from '../shared';

const DEFAULT_STATE = {
    nodeDataArray: [{ key: -1, category: "Start", color: paletteNodeColor.Start, label: 'Start' }],
    linkDataArray: []
};

const { action, getState: getDiagramModelState } = namespaceConfig('diagramModel', DEFAULT_STATE);

const {
    propertyAction: nodeDataAction,
    getPropertyState: getNodeDataState
} = staticPropertyConfig(action, 'nodeDataArray');

const {
    propertyAction: linkDataAction,
    getPropertyState: getLinkDataState
} = staticPropertyConfig(action, 'linkDataArray');

export {
    DEFAULT_STATE as diagramModel,
    nodeDataAction,
    linkDataAction,
    getNodeDataState,
    getLinkDataState,
    getDiagramModelState
};