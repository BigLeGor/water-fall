import { paletteModel } from './paletteModel';
import { diagramModel } from './diagramModel';
import { selectedNodeKeys } from './selectedNodeKeys';

export default {
    paletteModel,
    diagramModel,
    selectedNodeKeys
};