import { namespaceConfig } from 'fast-redux'

const namespace = 'selectedNodeKeys';

const DEFAULT_STATE = {};

const {
    action,
    getState: getSelectedNodeKeys
} = namespaceConfig(namespace, DEFAULT_STATE);

const bindSelectNodeReducer = reducer => action(namespace, reducer);

export {
    DEFAULT_STATE as selectedNodeKeys,
    bindSelectNodeReducer,
    getSelectedNodeKeys
};