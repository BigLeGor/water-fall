import express from 'express';
import dotenv from 'dotenv';
import path from 'path';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from '../../webpack/dev.config.js';
import { loadDealData } from './dealData';
import { loadWaterfall } from './waterfall';

const app = express();
const DIST_DIR = __dirname;
const HTML_FILE = path.join(DIST_DIR, 'index.html');
const compiler = webpack(config);
dotenv.config({ path: path.resolve(DIST_DIR, '../env/dev') });

app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath
}));

app.use(webpackHotMiddleware(compiler));

app.get('/', (_req, res, next) => {
    compiler.outputFileSystem.readFile(HTML_FILE, (err, result) => {
        if (err) {
            return next(err)
        }
        res.set('content-type', 'text/html')
        res.send(result)
        res.end()
    })
});

app.get('/api/deal-data', async (_req, res) => {
    const data = await loadDealData();
    res.send(data);
});


app.get('/api/waterfall', async (_req, res) => {
    const data = await loadWaterfall();
    res.send(data);
});


const { PORT: port = 3000 } = process.env;
app.listen(port, () => console.log(`server listening on port ${port}!`));