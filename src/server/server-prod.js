import path from 'path'
import express from 'express'
import myDiagram from './goDiagram';
import dotenv from 'dotenv';

const app = express();
const DIST_DIR = __dirname;
const HTML_FILE = path.join(DIST_DIR, 'index.html');
dotenv.config({ path: path.resolve(DIST_DIR, '../env/prod') });

app.use(express.static(DIST_DIR));

app.get('/', (req, res) => res.sendFile(HTML_FILE));

myDiagram.addDiagramListener('InitialLayoutCompleted', 
    () => app.get('/api/gojs', (req, res) => res.send(myDiagram.model.toJSON()))
);


const { PORT: port = 3000 } = process.env;
app.listen(port, () => console.log(`server listening on port ${port}!`));