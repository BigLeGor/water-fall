import fs from "fs";
import path from "path";
import shortid from "shortid";

export const readFile = filePath => new Promise(
    (resolve, reject) => fs.readFile(filePath, "utf8",
        (err, data) => err ? reject(err) : resolve(data)
    )
);

/*[  // specify the contents of the Palette
                { category: "Start", text: "Start" },
                { text: "Step" },
                { category: "Conditional", text: "???" },
                { category: "End", text: "End" },
                { category: "Comment", text: "Comment" }
            ]
 */

const buildPaletteModel = dealDataJson =>
    Object.entries(dealDataJson).reduce((acc, [key, val]) => {
        if (!key.includes("Block")) {
            return acc;
        }

        return [
            ...acc,
            ...val.map(({ BlockType: blockType, Label, ...props }) => ({
                id: shortid.generate(),
                category: key.replace("Block", ""),
                blockType,
                label: Label || "???",
                props
            }))
        ]
    }, []);

export const loadDealData = async () => {
    let dealData = {};
    try {
        const res = await readFile(path.join(__dirname, "../mock-data/Deal Data for UI.json"));
        dealData = buildPaletteModel(JSON.parse(res));
    } catch (ex) {
        console.log(ex)
    }
    return dealData;
};