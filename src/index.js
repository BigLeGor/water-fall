import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import { rootReducer } from 'fast-redux'
import go from 'gojs';

import App from './js/App';
import AppButtons from './js/components/AppButtons';
import preloadedState from './js/stores';
import './css/index.css';

const gojsKey = process.env.REACT_APP_GOJS_KEY;

if (gojsKey) {
    go.licenseKey = gojsKey;
}

const store = createStore(rootReducer, preloadedState, composeWithDevTools(applyMiddleware(thunkMiddleware)))

render(
    <Provider store={store}>
        <AppButtons />
        <App />
    </Provider>,
    document.getElementById('root')
);

if (module.hot) {
    module.hot.accept();
}