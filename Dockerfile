FROM node:lts-alpine

WORKDIR /usr/app/water-fall
COPY . ./
RUN npm i && npm run build:dev && npm cache clean -f
CMD ["npm", "run", "server:debug"]